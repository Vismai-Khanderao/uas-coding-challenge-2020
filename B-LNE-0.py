from shapely.geometry import LineString
from shapely.geometry import Point, MultiPoint
import numpy as np

def resolve(coord1, coord2, center, r):
    line = LineString([coord1, coord2])
    circle = Point(center[0],center[1]).buffer(r).boundary
    intersection = circle.intersection(line)
    print(intersection)
    data = {"Intersections" : 0,
            "Intersection Points" : [],
            "New Path" : []}
    line_list = [coord1]

    if isinstance(intersection, Point):
        data["Intersection Points"] = [(intersection.x, intersection.y)]
        v = np.array([intersection.x - center[0], intersection.y - center[1]])
        p = Point(center[0] + 1.1*v[0], center[1] + 1.1*v[1])
        line_list.append((p.x, p.y))
        data["Intersections"] = 1

    elif isinstance(intersection, MultiPoint):
        data["Intersection Points"] = [(intersection[0].x, intersection[0].y), (intersection[1].x, intersection[1].y)]
        v = np.array([coord2[0] - coord1[0], coord2[1] - coord1[1]])
        v_orth = np.array([-v[1],v[0]])
        v_orth_norm = v_orth/np.linalg.norm(v_orth)

        p1 = Point(intersection[0].x + 1.1*r*v_orth_norm[0], intersection[0].y + 1.1*r*v_orth_norm[1])
        p2 = Point(intersection[1].x + 1.1*r*v_orth_norm[0], intersection[1].y + 1.1*r*v_orth_norm[1])

        line_list.extend([(p1.x, p1.y), (p2.x, p2.y)])
        data["Intersections"] = 2

    line_list.append(coord2)

    data["New Path"] = line_list
    return data

i0 = resolve((0,-10),(15,15),(10,-5),4)
i1 = resolve((0,10),(30,10),(12,0),10)
i2 = resolve((0,-10),(15,15),(9,3),5)

x0 = resolve((-10,0),(-5,0), (0,0), 5)


print(i0)
print(i1)
print(i2)
print(x0)
print("testing")
print(resolve((0, -10), (0.8870115841339636, 2.168431114106325), (9,3),5))
print(resolve((0.8870115841339636, 2.168431114106325), (5.915833529161761, 10.549801022485987), (9,3),5))
print(resolve((5.915833529161761, 10.549801022485987), (15, 15), (9,3),5))