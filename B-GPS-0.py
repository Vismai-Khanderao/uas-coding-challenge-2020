import utm

f = open("missionwaypoints.txt", "r")
time = -1
coords = []
for line in f:
    time+=1
    coords_line = line[:-1]
    coords_line = coords_line.split("\t")
    utm_coords_line = utm.from_latlon(float(coords_line[0]), float(coords_line[1]))
    coords.append((utm_coords_line[0], utm_coords_line[1]))
    if len(coords) > 1:
        speed = ((coords[-2][0] - coords[-1][0])**2 + (coords[-2][1] - coords[-1][1])**2)**0.5
        print("Speed at {}s: {}".format(time,speed))

dist_moved = ((coords[-1][0] - coords[0][0])**2 + (coords[-1][1] - coords[0][1])**2)**0.5
print("Average speed: {}".format(dist_moved/time))
f.close()